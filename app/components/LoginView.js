import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  TextInput,
} from 'react-native';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Fumi} from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {color} from 'react-native-reanimated';
import {Sae} from 'react-native-textinput-effects';

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textUser: '',
      textPassword: '',
      count: 0,
      icon: 'eye-slash',
      password: true,
      data: {
        user: 'george',
        password: '123456',
      },
    };
  }
  changeTextInput = text => {
    this.setState({textUser: text});
  };
  textPassword(text) {
    this.setState({textPassword: text});
  }
  changeIcon() {
    this.setState(prevState => ({
      icon: prevState.icon === 'eye' ? 'eye-slash' : 'eye',
      password: !prevState.password,
    }));
  }
  onLogin() {
    this.state.textUser === this.state.data.user &&
    this.state.textPassword === this.state.data.password
      ? this.props.navigation.reset({
          index: 0,
          routes: [
            {
              name: 'User',
            },
          ],
        })
      : Alert.alert('Error', 'Try again');
    console.log(this.state.textPassword, this.state.data.password);
  }
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={{
            uri:
              'https://www.pngkey.com/png/full/432-4327458_peliculas-icono-png-film.png',
          }}
          style={{
            width: 200,
            height: 200,
            alignSelf: 'center',
          }}
        />

        <Fumi
          label={'Username'}
          iconClass={FontAwesomeIcon}
          iconName={'user'}
          iconColor={'white'}
          iconSize={25}
          style={{
            backgroundColor: '#262626',
            marginTop: 20,
            borderRadius: 10,
          }}
          labelStyle={{color: 'gray', fontSize: 15}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textUser}
        />
        <View
          style={{
            flexDirection: 'row',
            marginTop: 20,
            backgroundColor: '#262626',
            borderRadius: 10,
            alignItems: 'center',
            paddingRight: 20,
          }}>
          <Fumi
            label={'Password'}
            iconClass={FontAwesomeIcon}
            iconName={'lock'}
            iconColor={'white'}
            iconSize={25}
            style={{
              borderRadius: 10,
              backgroundColor: '#262626',
              flex: 1,
            }}
            labelStyle={{color: 'gray', fontSize: 15}}
            onChangeText={this.textPassword.bind(this)}
            value={this.state.textPassword}
            secureTextEntry={this.state.password}
          />
          <Icon
            name={this.state.icon}
            size={24}
            color={'white'}
            onPress={this.changeIcon.bind(this)}
          />
        </View>

        <TouchableOpacity
          onPress={this.onLogin.bind(this)}
          style={[
            this.state.textUser.length > 0 && this.state.textPassword.length > 0
              ? styles.buttonFinal
              : styles.buttonInitial,
          ]}>
          <Text style={styles.textButton}>Iniciar sesión</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1c1c1c',
    flex: 1,
    /*  alignSelf: 'stretch', */
    alignItems: 'stretch',
    padding: 20,
  },
  TextInput: {
    backgroundColor: '#262626',
    paddingRight: 20,
    paddingLeft: 10,
    borderBottomWidth: 0,
    borderRadius: 10,
    marginRight: -10,
    marginLeft: -10,
    marginTop: 20,
    padding: 5,
  },
  writeInput: {
    paddingStart: 15,
    color: 'white',
  },
  buttonInitial: {
    marginTop: 20,
    borderWidth: 1.5,
    borderColor: 'white',
    padding: 15,
    borderRadius: 10,
  },
  buttonFinal: {
    borderWidth: 1.5,
    borderColor: 'red',
    backgroundColor: 'red',
    marginTop: 20,
    padding: 15,
    borderRadius: 10,
  },
  textButton: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 20,
  },
});
export default LoginView;

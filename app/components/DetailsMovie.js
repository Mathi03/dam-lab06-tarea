import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Alert,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
  ShineOverlay,
} from 'rn-placeholder';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

export default class DetailsMovie extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0,
      items: [],
      error: null,
      enviado: this.props.route.params.itemId,
      dataFecth: false,
    };
  }

  async componentDidMount() {
    await fetch(
      `https://yts.mx/api/v2/movie_details.json?movie_id=${this.state.enviado}`,
    )
      .then(res => res.json())
      .then(
        result => {
          /* console.warn('result', result.data.movie); */
          this.setState({
            items: result.data.movie,
          });
          this.setState({dataFecth: true});
          console.log(this.state.dataFecth);
        },
        error => {
          this.setState({
            error: error,
          });
        },
      );
  }

  render() {
    return this.state.dataFecth === false ? (
      <View style={styles.container}>
        <Placeholder Animation={ShineOverlay}>
          <PlaceholderMedia style={styles.image} />
          <PlaceholderMedia style={styles.backgroundImage} />
          <View style={styles.option}>
            <PlaceholderMedia style={styles.iconOption} />
            <PlaceholderMedia style={styles.iconOption} />
            <PlaceholderMedia style={styles.iconOption} />
          </View>
          <PlaceholderLine style={styles.title} />
          <PlaceholderLine style={styles.description} />
          <PlaceholderLine style={styles.description} />
          <PlaceholderLine style={styles.description} />
          <PlaceholderLine style={styles.genres} />
        </Placeholder>
      </View>
    ) : (
      <ScrollView style={styles.container}>
        <View>
          <Image
            source={{uri: this.state.items.background_image}}
            style={styles.backgroundImage}
            resizeMode="cover"
          />
          <Image
            source={{uri: this.state.items.medium_cover_image}}
            style={styles.image}
            resizeMode="cover"
          />
          <View style={styles.option}>
            <View>
              <Icon
                size={40}
                name="like1"
                color={'white'}
                style={styles.iconOption}
              />
              <Text
                style={{
                  color: 'white',
                  alignSelf: 'center',
                  marginLeft: 15,
                }}>
                Like
              </Text>
            </View>
            <View>
              <Icon
                size={40}
                name="sharealt"
                color={'white'}
                style={styles.iconOption}
              />
              <Text
                style={{
                  color: 'white',
                  alignSelf: 'center',
                  marginLeft: 15,
                }}>
                Shared
              </Text>
            </View>

            <View>
              <IconMaterial
                size={40}
                name="add"
                color={'white'}
                style={styles.iconOption}
              />
              <Text
                style={{
                  color: 'white',
                  alignSelf: 'center',
                  marginLeft: 15,
                }}>
                Add
              </Text>
            </View>
          </View>
          <Text style={styles.title}>{this.state.items.title_long}</Text>
          <Text style={styles.description}>
            {this.state.items.description_intro}
          </Text>
          <Text style={styles.genres}>Genres: {this.state.items.genres}</Text>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#202020',
  },
  backgroundImage: {
    width: responsiveWidth(100),
    height: responsiveHeight(25),
  },
  image: {
    width: responsiveWidth(50),
    height: responsiveHeight(30),
    position: 'absolute',
    left: 20,
    top: 40,
  },
  option: {
    flexDirection: 'row-reverse',
    paddingTop: 20,
    paddingLeft: 20,
  },
  iconOption: {
    marginLeft: 15,
  },
  title: {
    color: 'white',
    fontSize: 20,
    paddingTop: 35,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 15,
    color: 'white',
    paddingTop: 15,
  },
  genres: {
    fontSize: 15,
    color: 'white',
    paddingTop: 15,
  },
});

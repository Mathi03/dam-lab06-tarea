import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';

import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
import Video from 'react-native-video';
import {Icon} from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';

export class VideoView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rate: 1,
      volume: 1,
      muted: false,
      resizeMode: 'contain',
      duration: 0.0,
      currentTime: 0.0,
      controls: false,
      paused: true,
      skin: 'custom',
      ignoreSilentSwitch: null,
      isBuffering: false,
      iconVideo: 'playcircleo',
    };
  }
  onProgress(data) {
    this.setState({currentTime: data.currentTime});
  }
  renderVolumeControl(volume) {
    const isSelected = this.state.volume == volume;
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({volume: volume});
        }}>
        <Text
          style={[
            styles.controlOption,
            {fontWeight: isSelected ? 'bold' : 'normal'},
          ]}>
          {volume * 100}%
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'black',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({paused: !this.state.paused});
            this.setState({
              iconVideo:
                this.state.paused === true ? 'pausecircleo' : 'playcircleo',
            });
          }}
          style={{justifyContent: 'center'}}>
          <Video
            source={require('../video/NovemberRain.mp4')} // Can be a URL or a local file.
            style={{
              height: responsiveHeight(25),
              width: responsiveWidth(100),
              backgroundColor: 'black',
            }}
            resizeMode="cover"
            paused={this.state.paused}
            volume={this.state.volume}
          />
          <AntDesign
            name={this.state.iconVideo}
            color={'white'}
            size={26}
            style={{
              position: 'absolute',
              alignSelf: 'center',
            }}
          />
        </TouchableOpacity>

        <View style={styles.volumeControl}>
          {this.renderVolumeControl(0.5)}
          {this.renderVolumeControl(1)}
          {this.renderVolumeControl(1.5)}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  controlOption: {
    alignSelf: 'center',
    fontSize: 20,
    color: 'white',
    paddingLeft: 2,
    paddingRight: 2,
  },
  volumeControl: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default VideoView;

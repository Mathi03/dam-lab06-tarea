import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Alert,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
  ShineOverlay,
} from 'rn-placeholder';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

export default class ListMovies extends Component {
  constructor(props) {
    super(props);

    this.state = {
      booksList: [...new Array(10).fill({})],
      textValue: 0,
      count: 0,
      items: [],
      error: null,
      dataFecth: false,
    };
  }

  itemRender = ({item}) => (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('DetailsMovie', {
            itemId: item.id,
            itemTitle: item.title,
          });
        }}>
        <View style={styles.itemList}>
          <View>
            <Image
              source={{
                uri: item.medium_cover_image,
              }}
              style={styles.imageItem}
            />
          </View>
          <View style={styles.descrition}>
            <Text numberOfLines={1} style={styles.titleItem}>
              {item.title}
            </Text>
            <Text numberOfLines={3} style={styles.subtitleItem}>
              {item.synopsis}
            </Text>
          </View>
          <Icon size={30} name="chevron-right" color={'white'} />
        </View>
      </TouchableOpacity>
    </View>
  );

  placerHolder = () => (
    <View style={styles.container}>
      <Placeholder
        Animation={Fade}
        style={styles.itemList}
        Left={props => <PlaceholderMedia style={styles.imageItem} />}
        Right={props => <PlaceholderMedia />}>
        <View style={styles.descrition}>
          <PlaceholderLine style={styles.titleItem} />
          <PlaceholderLine style={styles.subtitleItem} />
        </View>
      </Placeholder>
    </View>
  );

  async componentDidMount() {
    await fetch('https://yts.mx/api/v2/list_movies.json')
      .then(res => res.json())
      .then(
        result => {
          /* console.warn('result', result.data.movies); */
          this.setState({
            items: result.data.movies,
          });
          this.setState({dataFecth: true});
          console.log(this.state.dataFecth);
        },
        error => {
          this.setState({
            error: error,
          });
        },
      );
  }

  render() {
    return (
      <View>
        <FlatList
          data={this.state.items.length > 0 ? this.state.items : []}
          renderItem={
            this.state.dataFecth === false ? this.placerHolder : this.itemRender
          }
          keyExtractor={item => item.id.toString()}
          style={{backgroundColor: 'black'}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#202020',
    paddingRight: 10,
    paddingLeft: 10,
  },
  itemList: {
    flex: 1,
    flexDirection: 'row',
    /* backgroundColor: 'red', */
    borderBottomWidth: 1,
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#202020',
    borderBottomWidth: 1.2,
    borderBottomColor: 'white',
    borderRadius: 25,
  },
  imageItem: {
    width: 80,
    height: 80,
    borderRadius: 100,
  },
  descrition: {
    flex: 1,
    alignItems: 'stretch',
    paddingLeft: 20,
    paddingRight: 20,
  },
  titleItem: {
    /* backgroundColor: 'blue', */
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white',
  },
  subtitleItem: {
    /* backgroundColor: 'green', */
    color: 'gray',
    paddingTop: 10,
    textAlign: 'justify',
    fontFamily: 'arial',
  },
});

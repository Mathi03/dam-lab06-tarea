import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import ListMovies from './app/components/ListMovies';
import DetailsMovie from './app/components/DetailsMovie';
import LoginView from './app/components/LoginView';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import AntDesign from 'react-native-vector-icons/AntDesign';

import Foundation from 'react-native-vector-icons/Foundation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import VideoView from './app/components/VideoView';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();
const InicioStack = createStackNavigator();

function Inicio() {
  return (
    <InicioStack.Navigator
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <InicioStack.Screen
        name="ListMovies"
        component={ListMovies}
        options={{
          title: 'Lista de Peliculas',
          headerStyle: {
            backgroundColor: 'red',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <InicioStack.Screen
        name="DetailsMovie"
        component={DetailsMovie}
        options={({route}) => ({
          title: route.params.itemTitle,
          headerStyle: {
            backgroundColor: 'red',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </InicioStack.Navigator>
  );
}

function User() {
  return (
    <Tab.Navigator
      activeColor="white"
      inactiveColor="gray"
      barStyle={{backgroundColor: '#1c1c1c'}}>
      <Tab.Screen
        name="Inicio"
        component={Inicio}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesome name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Buscar"
        component={Buscar}
        options={{
          tabBarIcon: ({color}) => (
            <AntDesign name="search1" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Video"
        component={VideoView}
        options={{
          tabBarIcon: ({color}) => (
            <Foundation name="play-video" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
function Buscar() {
  return (
    <View>
      <Text>Buscar</Text>
    </View>
  );
}
function Video() {
  return (
    <View>
      <Text>Video</Text>
    </View>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={LoginView}
          options={{
            title: 'RePelis',
            headerStyle: {
              backgroundColor: 'red',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="User"
          component={User}
          options={{
            title: 'Usuario',
            headerStyle: {
              backgroundColor: 'red',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
